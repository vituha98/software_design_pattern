The immutable data of an object is recognized as "internal state". All other data is "external state". 

The Lightweight pattern suggests not storing external state in the class, but passing it to certain methods through parameters. Thus, the same objects can be reused in different contexts. But the main thing is that you will need much fewer objects, because now they will differ only in their internal state, and it does not have many variations.

-- External State Store 

But a more elegant solution would be to create an additional context class that would associate the external state with one or another lightweight. This will allow you to get by with only one array field in the container class. 

-- Lightweight Immutability

Since lightweight objects will be used in different contexts, you must ensure that their state cannot be changed after creation. The lightweight must receive all internal state through constructor parameters. It should not have setters and public fields. 

-- Lightweight Factory
For the convenience of working with lightweights and contexts, you can create a factory method that takes in parameters all the internal (and sometimes external) state of the desired object.

The main benefit of this method is to look for already created lightweights with the same internal state as the desired one. If a lightweight is found, it can be reused. If not, just create a new one. Usually, this method is added to the lightweight container or a separate factory class is created. It can even be made static and placed in the lightweight class.

 APPLICABILITY
When there is not enough RAM to support all the necessary objects.

The effectiveness of the Lightweight pattern largely depends on how and where it is used. Apply this pattern when all of the following conditions are met:

. the application uses a large number of objects;
. because of this, the cost of RAM is high;
. most of the state of objects can be moved outside of their classes;
. large groups of objects can be replaced with a relatively small number of shared objects because the external state is rendered. 

ADVANTAGES and DISADVANTAGES
Saves RAM.    Spends CPU time searching/computing the context.
              Complicates the program code due to the introduction of many additional classes.