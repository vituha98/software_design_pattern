import 'dart:convert';

import 'package:equatable/equatable.dart';

class PizzaOrderFlyWeight extends Equatable {
  final String pizzaSize;
  final int diameter;
  PizzaOrderFlyWeight(
    this.pizzaSize,
    this.diameter,
  );
  PizzaOrderFlyWeight.fromOrder(PizzaOrderFlyWeight sharedState)
      : this(sharedState.pizzaSize, sharedState.diameter);

  @override
  List<Object> get props => [pizzaSize, diameter];

  @override
  String toString() =>
      'PizzaOrderFlyWeight(pizzaSize: $pizzaSize, diameter: $diameter)';
}

class PizzaOrderContext {
  final String name;
  final PizzaOrderFlyWeight sharedState;

  PizzaOrderContext(this.name, this.sharedState);

  @override
  String toString() => 'unique state: $name || shared state: $sharedState';
}

class FlyWeightFactory {
  var flyweights = <PizzaOrderFlyWeight?>[];

  PizzaOrderFlyWeight getFlyWeight(PizzaOrderFlyWeight sharedState) {
    var state =
        flyweights.firstWhere((element) => sharedState == element, orElse: () {
      return null;
    });
    if (state == null) {
      state = sharedState;
      flyweights.add(sharedState);
    }
    return state;
  }

  int get total => flyweights.length;
}

class PizzaOrderMaker {
  final FlyWeightFactory _flyWeightFactory;

  PizzaOrderMaker(this._flyWeightFactory);

  PizzaOrderContext makePizzaOrder(
      String uniqueState, PizzaOrderFlyWeight sharedState) {
    var flyWeight = _flyWeightFactory.getFlyWeight(sharedState);
    return PizzaOrderContext(uniqueState, flyWeight);
  }
}

void main() {
  var flyweightFactory = FlyWeightFactory();
  var pizzaMaker = PizzaOrderMaker(flyweightFactory);
  var sharedState = <PizzaOrderFlyWeight>[
    PizzaOrderFlyWeight('Big pizza', 30),
    PizzaOrderFlyWeight('Medium pizza', 20),
    PizzaOrderFlyWeight('Small pizza', 15),
  ];
  var uniqueStates = <String>['Margarita', 'Salami', '4 Cheese'];

  var orders = [
    for (var name in uniqueStates)
      for (var shState in sharedState)
        {pizzaMaker.makePizzaOrder(name, shState)}
  ];
  print('Number of pizzas: ${orders.length}');
  print('Number of sharedState: ${flyweightFactory.total}');
  orders.asMap().forEach((key, value) {
    print('-' * 20);
    print('Pizza number in the list: ${key + 1}');
    print(value);
  });
}
